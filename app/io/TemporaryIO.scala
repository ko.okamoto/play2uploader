package io


import play.api.libs._


class TemporaryIO(val temporaryFile: Files.TemporaryFile) {

  def toBinary: Array[Byte]  
    = java.nio.file.Files.readAllBytes(temporaryFile.toPath()).toArray

  def getHeader: Array[Byte] = toBinary.slice(0, 8)

  def toHexString: String = toBinary.map("%02X".format(_)).mkString

  def moveTo(file: java.io.File): java.nio.file.Path = temporaryFile.moveTo(file)
}
