package controllers

import io._

import java.io.File
import java.nio.file.Files

import akka.stream.scaladsl.FileIO

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.http.{ HttpEntity, FileMimeTypes }



@Singleton
class StreamController @Inject()(cc: ControllerComponents, env: Environment, mt: FileMimeTypes) extends AbstractController(cc) {

  def assets(file: String) = Action { implicit request =>
    val fp = new File(s"${env.rootPath}/public${request.path}")
    val path = fp.toPath

    if (!fp.exists) Forbidden
    else {
      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(FileIO.fromPath(path), Some(Files.size(path)), mt.forFileName(path.toString)))
    }
  }
}
