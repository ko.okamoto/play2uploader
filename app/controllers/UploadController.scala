package controllers

import io._

import java.io.File
import java.nio.file.Files

import javax.inject._
import play.api._
import play.api.mvc._


@Singleton
class UploadController @Inject()(cc: ControllerComponents, env: Environment) extends AbstractController(cc) {

  def index() = Action { implicit request: Request[AnyContent] => 
    Ok(s"curl --upload-file <FILE> http://${request.host}/<FILENAME>")
  }

  def upload(file: String) = Action(parse.temporaryFile) { implicit request =>

    val tio = new TemporaryIO(request.body)
    
    val sec = if (isImage(tio.getHeader)) "assets" else "files"
    tio.moveTo(new File(s"public/$sec/$file"))
    Ok(s"http://${request.host}/$sec/$file")
  }


  private val imr = "89504E470D0A1A0A|^FFD8.*|^424D.*".r

  private def isImage(header: Array[Byte]): Boolean
    = imr.findFirstIn(header.map("%02X".format(_)).mkString).nonEmpty
}
